
from django import forms
from django.forms import ModelForm
from .models import SignUp

class SignUpForm(forms.ModelForm):
	date = forms.DateField(label = "Tanggal Lahir", required = True, widget = forms.DateInput(attrs = {'class' : 'form-control', 'type' : 'date'}))
	password = forms.CharField(widget=forms.PasswordInput)

	class Meta:
		model = SignUp
		fields = ['name', 'date', 'email', 'password']
        #ulang kayak awal