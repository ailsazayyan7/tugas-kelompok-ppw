from django.shortcuts import render, reverse
from .models import SignUp
from .forms import SignUpForm
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.core.exceptions import MultipleObjectsReturned
from django.contrib.auth import logout
# Create your views here.

response = {'author' : 'D10'}
def base(request):
    response = {'web_title' : 'Base'}
    return render(request, 'base.html', response)

def home(request):
    response = {'web_title' : 'Home'}
    return render(request, 'home.html', response)

def process(request):
	response = {'web_title': 'process'}
	return render(request, 'process.html', response)

EMAIL_HAS_BEEN_USED = "e-mail has been used"
# def signup(request):
# 	# user = SignUp.objects.all().values()

# 	# if request.method == "POST":
# 	# 	try:
# 	# 		SignUp.objects.get(email=request.POST['email'])
# 	# 		return HttpResponseServerError(EMAIL_HAS_BEEN_USED)
# 	# 	except SignUp.DoesNotExist:
# 	# 		pass
# 	# 	except MultipleObjectsReturned:
# 	# 		return HttpResponseServerError(EMAIL_HAS_BEEN_USED)
# 	# 	form = SignUpForm(request.POST)
# 	# 	if form.is_valid():
# 	# 		form.save()
# 	# 		return HttpResponseRedirect(reverse('signup'))

# 	# else:
# 	# 	form = SignUpForm

# 	# response = {'register':form, 'user':user}
# 	html = 'signup1.html'
# 	return render (request, html, response)

def signup(request):
	html = 'signup1.html'
	if request.user.is_authenticated:
		if 'name' not in request.session:
			request.session['name'] = request.user.first_name + " " + request.user.last_name
			print(request.session['name'])
		if 'email' not in request.session:
			request.session['email'] = request.user.email
		if 'username' not in request.session:
			request.session['username'] = request.user.username
	return render(request, html, response)

def logoutView(request):
	request.session.flush()
	print(dict(request.session))
	logout(request)
	return HttpResponseRedirect('/')
