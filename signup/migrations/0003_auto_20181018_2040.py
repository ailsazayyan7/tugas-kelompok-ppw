# Generated by Django 2.1.2 on 2018-10-18 13:40

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('signup', '0002_auto_20181018_1138'),
    ]

    operations = [
        migrations.RenameField(
            model_name='signup',
            old_name='nama_lengkap',
            new_name='name',
        ),
        migrations.RemoveField(
            model_name='signup',
            name='tanggal_lahir',
        ),
        migrations.AddField(
            model_name='signup',
            name='birth_date',
            field=models.DateField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
