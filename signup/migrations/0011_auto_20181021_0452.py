# Generated by Django 2.1.1 on 2018-10-20 21:52

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('signup', '0010_auto_20181020_1318'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='signup',
            options={},
        ),
        migrations.RemoveField(
            model_name='signup',
            name='date',
        ),
        migrations.RemoveField(
            model_name='signup',
            name='name',
        ),
        migrations.AddField(
            model_name='signup',
            name='nama_lengkap',
            field=models.CharField(default='-', max_length=27),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='signup',
            name='tanggal_lahir',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
