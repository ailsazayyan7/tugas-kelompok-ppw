# Generated by Django 2.1.2 on 2018-10-19 11:51

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('signup', '0004_auto_20181018_2142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='signup',
            name='birth_date',
            field=models.DateField(default=datetime.datetime(2018, 10, 19, 11, 51, 29, 321127, tzinfo=utc)),
        ),
    ]
