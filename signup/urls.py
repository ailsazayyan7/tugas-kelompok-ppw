from django.urls import path
from .views import base, home, signup, process, logoutView
from django.contrib.auth import logout

urlpatterns = [
    path("400", base, name="base"),
    path("", signup, name="signup"),
    path("process", process, name="process"),
    path("logout", logoutView, name = "logout"),

    
]