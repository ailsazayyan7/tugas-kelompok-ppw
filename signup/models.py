from django.db import models
from datetime import date

# Create your models here.
class SignUp(models.Model):
	attrs = {
		'class': 'form-control'
	}

	name = models.CharField(max_length = 50)
	date = models.DateField()
	email = models.EmailField()
	password = models.CharField(max_length = 16)

	class Meta:
		verbose_name_plural = "Donators"

	def __str__(self):
		return self.name
	