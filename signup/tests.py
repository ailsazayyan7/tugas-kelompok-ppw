from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from django.urls import resolve
from django.http import HttpRequest
from .models import SignUp
from .views import signup


# Create your tests here.
class FormUnitTest(TestCase):

		def test_lab_sign_up_is_exist(self):
			response = Client().get('/signup/')
			self.assertEqual(response.status_code, 200)

		def test_signup_using_signup_func(self): #to remember: masuk is your views func and signup is your webpage
			found = resolve('/signup/')
			self.assertEqual(found.func, signup)

		def test_login_user(self):
			c = Client()
			c.force_login(User.objects.get_or_create(username='testuser')[0])
			home = c.get('/signup/')
			self.assertTrue(home.context['user'].is_authenticated)
			session = dict(c.session)
			self.assertTrue("username" in session)
			self.assertTrue("email" in session)
			self.assertTrue("name" in session)
			response = c.get('/signup/')
			self.assertIn("Halo", response.content.decode('utf8'))

		def test_logout_function(self):
			c = Client()
			c.force_login(User.objects.get_or_create(username='testuser')[0])
			c.logout()
			home = c.get('/signup/')
			self.assertFalse(home.context['user'].is_authenticated)
			session = dict(c.session)
			self.assertFalse("username" in session)
			self.assertFalse("email" in session)
			self.assertFalse("name" in session)
			response = c.get('/signup/')
			self.assertIn("Login with Google", response.content.decode('utf8'))


		# def test_model_can_create_new_user(self):
		# 	#creating a new activity
		# 	new_user = SignUp.objects.create(nama_lengkap = 'ailsa')

		# 	#retrieving all available activity
		# 	counting_all_available_user=SignUp.objects.all().count()
		# 	self.assertEqual(counting_all_available_user, 1)

		

# '''
# 		def test_if_there_is_Hello_apa_kabar(self):
# 			request = HttpRequest()
# 			response = index(request)
# 			html_response = response.content.decode('utf8')
# 			self.assertIn("Hello, apa kabar?", html_response)

# 		def test_lab6_using_index_func(self):
# 			found = resolve('/lab6/')
# 			self.assertEqual(found.func, index)

# 		def test_model_can_create_new_status(self):
# 			#creating a new activity
# 			new_activity = Status.objects.create(my_status='the status')

# 			#retrieving all available activity
# 			counting_all_available_status=Status.objects.all().count()
# 			self.assertEqual(counting_all_available_status, 1)
# 			'''