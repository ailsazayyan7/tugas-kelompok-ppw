from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Donate
from .views import donate


# Create your tests here.
class DonateUnitTest(TestCase):

		def test_donate_is_exist(self):
			response = Client().get('/donate/')
			self.assertEqual(response.status_code, 302)

		def test_donate_using_donate_func(self): #to remember: signup is your views func and signup is your webpage
			found = resolve('/donate/')
			self.assertEqual(found.func, donate)

		def test_model_can_create_new_donator(self):
			#creating a new activity
			new_donator = Donate.objects.create(nama_donatur = "Yosua", email = "yosua1010101@gmail.com", jumlah_donasi = 1000000, tampilkan = True)

			#retrieving all available activity
			counting_all_available_donator=Donate.objects.all().count()
			self.assertEqual(counting_all_available_donator, 1)