from django.shortcuts import render, reverse
from .models import Donate
from .forms import DonateForm
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.

response = {}

@login_required
def donate(request):
	response = {}
	donator = Donate.objects.all().values()

	if request.method == "POST":
		form = DonateForm(request.POST)
		if form.is_valid():
			response['nama_program'] = request.POST['nama_program']
			response['nama_donatur'] = request.user.username
			response['email'] = request.user.email
			response['jumlah_donasi'] = request.POST['jumlah_donasi']

			orang_baik = Donate(nama_program = response['nama_program'], nama_donatur = response['nama_donatur'], email = response['email'],
				jumlah_donasi = response['jumlah_donasi'])

			orang_baik.save()
			response = {'web_title': 'Terima Kasih'}
			return render(request, 'terimaKasih.html', response)
			

	else:
		form = DonateForm

	if(request.user.is_authenticated != True):
		return HttpResponseRedirect('/signup/')

	response = {'donating':form , 'donator':donator}
	html = 'donate.html'
	return render (request, html, response)

def terimaKasih(request):
	response = {'web_title': 'Terima Kasih'}
	return render(request, 'terimaKasih.html', response)
