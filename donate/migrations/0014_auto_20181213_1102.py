# Generated by Django 2.1.1 on 2018-12-13 04:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('donate', '0013_auto_20181207_0034'),
    ]

    operations = [
        migrations.RenameField(
            model_name='donate',
            old_name='nama_donator',
            new_name='nama_donatur',
        ),
    ]
