from django.db import models
# Create your models here.
class Donate(models.Model):
	PAYMENT= [
            ('a', 'Transfer A'),
            ('b', 'Transfer B'),
            ('c', 'Transfer C'),
            ('d', 'Transfer D'),
            ('e', 'Transfer E')
        ]
	nama_program = models.CharField(max_length = 27)
	nama_donatur = models.CharField(max_length = 27)
	email = models.EmailField()
	jumlah_donasi = models.CharField(max_length = 30)
	metode_pembayaran = models.CharField(choices=PAYMENT, max_length=1, null=False)
	tampilkan = models.BooleanField(default = False)