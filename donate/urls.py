from django.urls import path
from .views import donate, terimaKasih

urlpatterns = [
    path("", donate, name="donate"),
    path("donate/done/", terimaKasih, name="terimaKasih"),
    path("donates", donate, name="donates")
]