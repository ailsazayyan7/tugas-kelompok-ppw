from django import forms
from .models import Donate
from django.db import models
from program.models import Program

class DonateForm(forms.Form):
    attrs = {
        'class':'form-control'
    }
    programs = Program.objects.all()
    PROGRAM = [prog.program_title for prog in programs]
    program_list = []
    for x in range(programs.count()):
        program_list = [(PROGRAM[x],PROGRAM[x])]

    nama_program = forms.ChoiceField(label="Pilih Program", choices=program_list, required=False)
    # nama_donatur = forms.CharField(label="Nama", max_length=50, required=False, widget = forms.TextInput(attrs=attrs))
    # email = forms.EmailField(label="Email", required=False, widget = forms.EmailInput(attrs=attrs))
    jumlah_donasi = forms.IntegerField(label="Jumlah Donasi", required=False, widget = forms.TextInput(attrs=attrs))
    tampilkan = forms.BooleanField(label="tampilkan", required=False)
