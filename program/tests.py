from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import program
from .models import Program
# Create your tests here.

class ProgramTest(TestCase):
	def test_program_url_is_exist(self):
		response = Client().get('/program/')
		self.assertEqual(response.status_code,200)

	def test_base_url_is_exist(self):
		response = Client().get('/') 
		self.assertEqual(response.status_code,200)

	def test_website_using_html(self):
		response = Client().get('/program/')
		self.assertTemplateUsed(response, 'program.html')

	def test_program_is_using_function(self):
		found = resolve('/program/')
		self.assertEqual(found.func, program)

	def create_object(self, program_title='only a test', image='https://image.ibb.co/cUVvff/103805782-situbondo-earthquake-map640-nc-2.jpg', donation='100', target_donation='1000'):
		return Program.objects.create(program_title=program_title, image=image, donation=donation, target_donation=target_donation)
	
	def test_object_creation(self):
		o = self.create_object()
		self.assertTrue(isinstance(o, Program))
		self.assertEqual(o.__str__(), o.program_title)

	# def test_program_views(self):
	# 	o = self.create_object()
	# 	url = reverse('program')
	# 	response = Client().get(url)

	# 	self.assertEqual(response.status_code, 200)
	# 	self.assertIn(o.program_title, response.content)
	#teeeeesssss
