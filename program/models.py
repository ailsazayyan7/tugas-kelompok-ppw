from django.db import models

# Create your models here.
class Program(models.Model):
	program_title = models.CharField(max_length=100)
	program_desc = models.CharField(max_length=5000)
	image = models.CharField(max_length=10000)
	donation = models.IntegerField(default=0)
	target_donation = models.IntegerField()

	def campaign_title(self):
		return self.program_title

	def campaign_image(self):
		return self.image

	def campaign_progress(self):
		return self.donation

	def campaign_target(self):
		return self.target_donation

	def __str__(self):
		return self.program_title