# Generated by Django 2.1.2 on 2018-10-20 12:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('program', '0008_auto_20181020_1553'),
    ]

    operations = [
        migrations.AlterField(
            model_name='program',
            name='donation',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='program',
            name='image',
            field=models.CharField(max_length=10000),
        ),
        migrations.AlterField(
            model_name='program',
            name='program_title',
            field=models.CharField(max_length=100),
        ),
    ]
