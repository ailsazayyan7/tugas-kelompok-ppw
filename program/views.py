from django.shortcuts import render
from .models import Program
from crowd_funding.views import base

# Create your views here.

def program(request):
	progress = 0
	prog = Program.objects.all()
	for i in prog:
		progress = (i.donation / i.target_donation) * 100
	response = {'prog' : prog , 'web_title':'Program', 'progressbar':progress}
	return render(request, 'program.html', response)