from django.urls import path
from .views import base, program

urlpatterns = [
    path('', program, name="program")
]
