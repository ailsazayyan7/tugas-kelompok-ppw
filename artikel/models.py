from django.db import models
from django.utils import timezone

# Create your models here.
class artikel(models.Model):
	judul = models.TextField(max_length=10000)
	isi = models.TextField(max_length=10000)
	image = models.CharField(max_length=1000)
	sumber = models.CharField(max_length=1000)
	tanggal_publikasi = models.DateTimeField(auto_created=True)

	def get_judul(self):
		return self.judul

	def get_image(self):
		return self.image

	class Meta:
		verbose_name_plural = "Articles"

