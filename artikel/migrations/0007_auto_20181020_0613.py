# Generated by Django 2.1.2 on 2018-10-19 23:13

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('artikel', '0006_auto_20181020_0551'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artikel',
            name='tanggal_publikasi',
            field=models.DateTimeField(verbose_name=datetime.datetime(2018, 10, 19, 23, 13, 18, 834754, tzinfo=utc)),
        ),
    ]
