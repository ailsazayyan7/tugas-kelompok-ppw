from django.test import TestCase, Client
from .views import news, terimaKasih
from django.http import HttpRequest

# Create your tests here.
class testTP(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/news')
		self.assertEqual(response.status_code, 301)

	def test_using_to_do_list_template(self):
		response = Client().get('/news/')
		self.assertTemplateUsed(response, 'artikel.html')

	# def test_using_to_do_list_template2(self):
	# 	response = Client().get('/done')
	# 	self.assertTemplateUsed(response, 'terimaKasih.html')