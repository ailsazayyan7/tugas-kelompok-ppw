from django.shortcuts import render
from artikel.models import artikel

# Create your views here.

def news(request):
	art = artikel.objects.all()
	response = {'art' : art, 'web_title': 'Article'}
	return render(request, 'artikel.html', response)

def terimaKasih(request):
	response = {}
	return render(request, 'terimaKasih.html', response)