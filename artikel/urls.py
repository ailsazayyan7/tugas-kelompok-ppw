from django.urls import path
from artikel.views import news, terimaKasih

urlpatterns = [
	path('', news, name='news'),
	path('terimakasih', terimaKasih, name='terimaKasih'),
]
