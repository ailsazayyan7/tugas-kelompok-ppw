from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from .models import Testimony
from .forms import TestimonyForm

# Create your views here.
team_member = ["Ailsa Zayyan Salsabila", 
               "Aurellia Dhya Andini", 
               "Thalia Talula Kesuma Putri", 
               "Yosua Krisnando Bagaskara"]

def base(request):
    response = {'web_title' : 'Base'}
    return render(request, 'base.html', response)

def about(request):
    response = {'web_title' : 'About', "members" : team_member, "testimony_form":TestimonyForm}
    return render(request, 'about.html', response)

def testimony_list(request):
    testimony = Testimony.objects.all().order_by("-created_date")
    response = {"data":list(testimony)}
    return JsonResponse(response)

def create_testimony(request):
    if request.method == "POST":
        this_name = request.POST["this_name"]
        comment = request.POST["comment"]
        tes = Testimony(this_name = this_name, comment = comment)
        tes.save()
        return HttpResponseRedirect('/about')