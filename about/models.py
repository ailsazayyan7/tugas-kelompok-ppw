from django.db import models
from django.utils import timezone
# Create your models here.

class Testimony(models.Model):
    
    this_name = models.CharField(max_length = 50)
    comment = models.CharField(max_length = 200)
    created_date = models.DateTimeField(auto_created=True)

    class Meta:
        verbose_name_plural = "Testimonies"

    def __str__(self):
        return "Message " + self.pk