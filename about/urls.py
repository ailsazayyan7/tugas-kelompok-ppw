from django.urls import path
from about.views import base, about, testimony_list, create_testimony

urlpatterns = [
    path("400", base, name="base"),
    path("", about, name="about"),
    path("testimony_json", testimony_list, name="testimony_json"),
    path("create", create_testimony, name="create")
]