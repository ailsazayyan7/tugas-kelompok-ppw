from django import forms

class TestimonyForm(forms.Form):
    attrs = {
        'class' : 'form-control'
    }

    message = forms.CharField(label="Testimony", max_length=50, required=False, widget=forms.TextInput(attrs=attrs))