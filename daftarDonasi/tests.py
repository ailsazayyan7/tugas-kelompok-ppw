from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import data
from .views import get_user_total_donasi
from django.http import HttpRequest

# Create your tests here.
class testDaftarDonasi(TestCase):
	def test_url_daftarDonasi_is_exist(self):
		response = Client().get('/daftarDonasi')
		self.assertEqual(response.status_code, 301)

	def test_using_to_do_list_template(self):
		response = Client().get('/daftarDonasi/')
		self.assertTemplateUsed(response, 'daftarDonasi.html')

	def test_function1(self):
		found = resolve('/daftarDonasi/')
		self.assertEqual(found.func, get_user_total_donasi)

	def test_function2(self):
		found = resolve('/daftarDonasi/data')
		self.assertEqual(found.func, data)

	# def test_donatur_template(self):
	# 	response = Client().get('/daftarDonasi/')
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn('Jumlah', html_response)
