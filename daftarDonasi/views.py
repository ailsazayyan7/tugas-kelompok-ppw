from django.shortcuts import render
from donate.models import Donate
import requests
from django.http import HttpResponseRedirect, JsonResponse
from django.http import HttpResponse
import json

# donasi1 = Donate(nama_program='program1', nama_donator='donatur1', email='donatur1@gmail.com', jumlah_donasi=1000, metode_pembayaran='a', tampilkan=True)
# donasi2 = Donate(nama_program='program2', nama_donator='donatur2', email='donatur2@gmail.com', jumlah_donasi=2000, metode_pembayaran='b', tampilkan=True)
# donasi1.save()
# donasi2.save()

# Create your views here.
def data(request):
	daftar = Donate.objects.all().values('nama_program', 'jumlah_donasi')
	listDaftar = list(daftar)
	
	return JsonResponse(listDaftar, safe=False)
	

	# jsonDict = {}
	# jsonList = []
	# daftar = Donate.objects.all().values('nama_program', 'jumlah_donasi')

	# for i in range(Donate.objects.all().count()):
	# 	jsonDict["objectDonasi"] = daftar[i]

	# return JsonResponse(jsonDict, safe=False)

def get_user_total_donasi(request):
	response = {}
	email = request.user.username + '@gmail.com'
	donasi = Donate.objects.filter(email=email)
	totalDonasi = 0
	for i in donasi:
		totalDonasi += int(i.jumlah_donasi)

	response['totalDonasi'] = totalDonasi

	return render(request, 'daftarDonasi.html', response)


# def daftarDonasi(request):
# 	response = {}
# 	email = request.user.username + '@gmail.com'
# 	listDonasi = Donate.objects.filter(email=email)

# 	totalDonasi = 0
# 	for i in listDonasi:
# 		totalDonasi += i.jumlah_donasi

# 	response = {'listDonasi' : listDonasi,
# 				'totalDonasi' : totalDonasi}
	
# 	return render(request, 'daftarDonasi.html', response)