from django.urls import path
from .views import data
from .views import get_user_total_donasi

urlpatterns = [
	path('', get_user_total_donasi, name='get_user_total_donasi'),
	path('data', data, name='data'),
]

