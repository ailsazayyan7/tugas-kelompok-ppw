from django.shortcuts import render
from artikel.models import artikel
from program.models import Program
from signup.models import SignUp

def base(request):
    response = {'web_title' : 'Base'}
    return render(request, 'base.html', response)

def home(request):
    donation_total = 0
    for i in Program.objects.all():
        donation_total += i.donation

    response = {'web_title' : 'Home', 
                'article_count' : artikel.objects.all().count(),
                'article' : artikel.objects.all().order_by('-tanggal_publikasi')[:1],
                'campaign_count' : Program.objects.all().count(),
                'campaign' : Program.objects.all().order_by('-target_donation')[:3],
                'donation_total' : donation_total,
                'donator_count' : SignUp.objects.all().count()}
    return render(request, 'home.html', response)