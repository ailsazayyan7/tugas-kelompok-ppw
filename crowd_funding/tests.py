from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from .forms import *
from .models import *
from .views import *
from .urls import *

# Create your tests here.
class UnitTest(TestCase):
    
    def test_website_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_base_template_exist(self):
        response = Client().get('/400')
        self.assertEqual(response.status_code, 200)

    def test_PPWTP_url_func_working(self):
        found = resolve('/')
        self.assertEqual(found.func, home)