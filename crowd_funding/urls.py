from django.urls import path
from .views import base, home

urlpatterns = [
    path("400", base, name="base"),
    path("", home, name="home")
]
